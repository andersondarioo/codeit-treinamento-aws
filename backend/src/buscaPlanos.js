const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({ region : 'us-east-2' });
const moment = require('moment');
const middy = require('middy')

const { cors } = require('middy/middlewares')

async function getPlanos(event) {
    let {destino, dataInicio, dataFim, dataNascimento} = JSON.parse(event.body);
    
    const dataInicioViagem = moment(dataInicio, 'DD/MM/YYYY');
    const dataFimViagem = moment(dataFim, 'DD/MM/YYYY');
    const dataNasc = moment(dataNascimento, 'DD/MM/YYYY');
    const duracaoViagem = dataFimViagem.diff(dataInicioViagem, 'days');
    const idade = moment().diff(dataNasc,  'years');
    
    const params = {
        TableName : 'listagemPlanos',
        FilterExpression : "destino = :dest",
        ExpressionAttributeValues: {
            ":dest": destino
        }
    };

    const data = await docClient.scan(params).promise();

    let planos = data.Items.filter(plano => (
        (plano.periodoVigenciaMinimo <= duracaoViagem)
        && (plano.periodoVigenciaMaximo >=  duracaoViagem)
        && (plano.idadeMinima <= idade)
        && (plano.idadeMaxima >= idade)
    ));


    const response = {
        statusCode: 200,
        body: JSON.stringify(planos),
    };

    return response;
};

module.exports.handle = middy(getPlanos).use(cors());