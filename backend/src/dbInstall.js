const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({ region : 'us-east-2' });

module.exports.handle = async () => {
    const jsonRaw = JSON.stringify(require('./listagemPlanos.json'));
    const planos = JSON.parse(jsonRaw);

    for (const plano of planos) {
        const params = { Item : plano, TableName: 'listagemPlanos' }
        await put(params);
    }

}

function put(params) {
    return new Promise((resolve, reject) => {
        docClient.put(params, (err, data) => err ? reject(err) : resolve("Insert data completed for planp " + params.Item.nome));
    });
}
