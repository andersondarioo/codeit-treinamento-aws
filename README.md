# codeit-treinamento-aws

### Requisitos
- Conta AWS
- Node.js
- Serverless

### Instalação do Serverless
```
npm install -g serverless
```

### Configurando Serverless
```
serverless config credentials -o --provider aws --key <YOUR_AWS_KEY> --secret <YOUR_AWS_SECRET>
```
A key e secret é obtido no serviço IAM da AWS. No momento da criação do usuário se obtem esses dados. Ou, durante a edição de um usuário existente, se obtem na seção "Credenciais de Segurança".


### Instalação de dependências
Entrar dentro de cada diretório (frontend e backend) e executar o seguinte comando:
```
npm install
```

### Deploy da aplicação
Ambos os deploys são realizados entrando dentro de cada diretório (frontend e backend) e executando o seguinte comando:
```
serverless deploy -v
```

Para o frontend, é necessário fazer o build da aplicação antes, isso é feito através da execução do comando:
```
npm run build
```

Para instalar a base, entrar no projeto do backend e executar o comando:
```
serverless invoke -f dbInstall
```

Para essa aplicação é interessante primeiramente fazer o deploy do backend. Após isso, pegar a url da API Gateway gerada, encontrada na aba "Estágios" da API criada, e substituir na aplicação frontend para depois realizar o deploy do frontend.

### Testando frontend localmente
Para testar o frontend localmente, basta executar o comando:
```
npm start
```

### Observação
Os buckets discriminados nas tags "deploymentBucket" precisam ser criados manualmente antes do deploy. É o bucket onde o serverless armazena os metadados do deploy. Caso não seja especificado essa tag, o framework irá criar um bucket com nome random. Para evitar isso, é colocado essa tag, porém a exigência é que o bucket em questão seja criado manualmente.