import React, { useState } from 'react';

import './App.css'

import moment from 'moment'

function getData(content) {
    const requestOptions = { method: 'POST', body: JSON.stringify(content)}
    return fetch('<API_URL>/planos/buscaPlanos', requestOptions)
}

function App() {
    const [origem, setorigem] = useState('')
    const [destino, setdestino] = useState('')
    const [dataInicio, setDataInicio] = useState('')
    const [dataFim, setDataFim] = useState('')
    const [dataNascimento, setDataNascimento] = useState('')
    const [tabelaItens, setTabelaItens] = useState([])

    const tableHead = ['Plano', 'Valor']

    return (
        <>
            <div>
                <label>Origem Viagem:  </label>
                <input type="text" value={origem} onChange={e => setorigem(e.target.value)} />
            </div>

            <div>
                <label>Destino Viagem:  </label>
                <input type="text" value={destino} onChange={e => setdestino(e.target.value)} />
            </div>

            <div>
                <label>Data início:  </label>
                <input type="date" value={dataInicio} onChange={e => setDataInicio(e.target.value)} />
            </div>

            <div>
                <label>Data fim:  </label>
                <input type="date" value={dataFim} onChange={e => setDataFim(e.target.value)} />
            </div>

            <div>
                <label>Data Nascimento:  </label>
                <input type="date" value={dataNascimento} onChange={e => setDataNascimento(e.target.value)} />
            </div>

            <div>
                <button onClick={() => 
                    getData({   origem, 
                                destino, 
                                dataInicio: moment(dataInicio).format('DD/MM/YYYY'), 
                                dataFim: moment(dataFim).format('DD/MM/YYYY'), 
                                dataNascimento: moment(dataNascimento).format('DD/MM/YYYY') 
                            })
                    .then(res => res.json())
                    .then(content => setTabelaItens(content))}>
                    Enviar
                </button>
            </div>

            <div>
                <table>
                    <thead>
                        <tr>{tableHead.map((name, i) => <th key={i}>{name}</th>)}</tr>
                    </thead>
                    <tbody>
                        {tabelaItens.map((item, i) => (
                            <tr key={i}>
                                <td>{item.nome}</td>
                                <td>R$ {item.preco}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default App;
